<?php

$doc = new DOMDocument;
$content_rss = file_get_contents('https://www.liga.net/news/sport/rss.xml');
$items_rss = new SimpleXmlElement($content_rss);
foreach ($items_rss->channel->item as $key => $item_rss) {

    preg_match_all('/type="?\'?image\/jpg"?\'?>\surl="?\'?([^"\']+)"?\'?[^>]*>\/?/i', $item_rss->description, $images);
    $link = preg_replace('/[a-z]+\.liga\.net/', 'sports.ru', $item_rss->link);

    echo '<img src="' . $item_rss->enclosure['url'] . '"><br>';
    echo '<h2>' . $item_rss->title . '</h2>';
    echo '<p>' . $item_rss->description . '</p><br><br>';
    echo 'Статья по ссылке: ' . '<a href="' . $item_rss->link . '">' . $link . '</a>';
    echo '<hr>';
}

